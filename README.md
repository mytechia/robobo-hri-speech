
# This repository moved. 
Please use the new location: https://github.com/mintforpeople/robobo-hri-speech

# ROBOBO Speech Library #

Contains the source code of the different modules that made up the ROBOBO Speech Library. 

This includes:

- Speech production module.
- Speech recognition module.

NOTE: All the development happens in the develop or feature branches. The master branch contains the last stable release.