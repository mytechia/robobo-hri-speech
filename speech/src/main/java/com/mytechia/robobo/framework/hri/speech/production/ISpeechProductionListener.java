package com.mytechia.robobo.framework.hri.speech.production;

/**
 * Created by luis on 28/6/17.
 */

public interface ISpeechProductionListener {
    public void onEndOfSpeech();
}
